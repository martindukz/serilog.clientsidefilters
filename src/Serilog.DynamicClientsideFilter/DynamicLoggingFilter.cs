﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog.DynamicClientsideFilter.Config;
using Serilog.Events;

namespace Serilog.DynamicClientsideFilter
{
    /// <summary>
    /// Class introducing extension methods. 
    /// </summary>
    public static class DynamicLoggingFilter
    {
        private static LogEventLevel _softLevel = LogEventLevel.Information;
        private static readonly ConcurrentDictionary<string, LogEventLevel> IdentifiedLevels = new ConcurrentDictionary<string, LogEventLevel>();

        private static readonly Dictionary<string, LogEventLevel> NamespacePrefixLogLevels = new Dictionary<string, LogEventLevel>();

        /// <summary>
        /// Apply log filter
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="logLevelConfigs"></param>
        /// <returns></returns>
        public static LoggerConfiguration ApplyDynamicFilter(this LoggerConfiguration configuration, IEnumerable<LogLevelConfig> logLevelConfigs = null)
        {
            var appliedFromArgument = ApplyFromArgument(logLevelConfigs);
            //Let configuration override "hardcoded"
            var appliedFromConfiguration = ApplyFromConfiguration();
            if (appliedFromConfiguration || appliedFromArgument)
            {
                configuration = configuration.Filter.ByIncludingOnly(ShouldIncludeLogEvent);
            }
            
            return configuration;
        }

        private static bool ApplyFromConfiguration()
        {
            LogFilterLevelsSection logLevelSection = ConfigurationManager.GetSection("LogFilterLevelsSection") as LogFilterLevelsSection;
            if (logLevelSection == null)
            {
                return false;
            }

            _softLevel = logLevelSection.SoftLogLevel ?? LogEventLevel.Information;

            var logLevelsFromConfig = logLevelSection.LogLevels?.OfType<LogLevelConfig>()?.ToList() ?? new List<LogLevelConfig>();
            ApplyLogLevels(logLevelsFromConfig); 
            return true; 
        }

        private static bool ApplyFromArgument(IEnumerable<LogLevelConfig> logLevelConfigs)
        {
            var logLevels = logLevelConfigs?.ToList() ?? new List<LogLevelConfig>();
            if (!logLevels.Any())
            {
                return false;
            }
            ApplyLogLevels(logLevels);
            return true; 
        }

        private static void ApplyLogLevels(List<LogLevelConfig> logLevels)
        {
            foreach (var logLevelPrefix in logLevels)
            {
                if (logLevelPrefix.SourceContextPrefix == null) continue;
                NamespacePrefixLogLevels[logLevelPrefix.SourceContextPrefix] = logLevelPrefix.Level;
            }
        }

        private static bool ShouldIncludeLogEvent(LogEvent le)
        {
            LogEventPropertyValue sourceContext;
            if (le.Properties == null || !le.Properties.TryGetValue("SourceContext", out sourceContext) || sourceContext == null)
            {
                return le.Level >= _softLevel;
            }
            var sourceContextString = sourceContext.ToString();
            if (string.IsNullOrWhiteSpace(sourceContextString))
            {
                return le.Level >= _softLevel;
            }

            LogEventLevel cutLevelForNameSpacePrefix = _softLevel;
            //Accepted concurrency issue: two threads can add the same key twice. No problem, because it uses Add or update.
            if (!IdentifiedLevels.ContainsKey(sourceContextString))
            {
                //remove quotation marks: 
                var withoutQuotes = sourceContextString.Substring(1, sourceContextString.Length - 2);

                var nameSpaceSplit = withoutQuotes.Split('.');
                if (nameSpaceSplit.Length == 0)
                {
                    return le.Level >= _softLevel;
                }

                //bad performance - should use "indexof(.) loop instead. 
                //Cache mitigates this small bad performance.
                var builtPrefix = "";

                for (int i = 0; i < nameSpaceSplit.Length; i++)
                {
                    //The most specific logsetting matching the namespace+type should win. 
                    builtPrefix += i > 0 ? "." + nameSpaceSplit[i] : nameSpaceSplit[i];

                    if (NamespacePrefixLogLevels.ContainsKey(builtPrefix))
                    {
                        cutLevelForNameSpacePrefix = NamespacePrefixLogLevels[builtPrefix];
                    }
                }

                IdentifiedLevels.AddOrUpdate(sourceContextString, cutLevelForNameSpacePrefix, (s, existing) => existing);
            }
            cutLevelForNameSpacePrefix = IdentifiedLevels[sourceContextString];

            return le.Level >= cutLevelForNameSpacePrefix;
        }

    }
}
