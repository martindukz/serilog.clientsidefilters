using System.Configuration;
using Serilog.Events;

namespace Serilog.DynamicClientsideFilter.Config
{
    public class LogFilterLevelsSection : ConfigurationSection
    {
        [ConfigurationProperty("softLogLevel", DefaultValue = LogEventLevel.Warning, IsRequired = false)]
        public LogEventLevel? SoftLogLevel
        {
            get
            {
                return (LogEventLevel)this["softLogLevel"];
            }
            set
            {
                this["softLogLevel"] = value;
            }
        }
        //Change to collections with Debug, Information, Warning, Error - to make it easy to configure. 
        //http://stackoverflow.com/questions/3935331/how-to-implement-a-configurationsection-with-a-configurationelementcollection
        [ConfigurationProperty("LogLevels", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(LogLevelConfigCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public LogLevelConfigCollection LogLevels
        {
            get
            {
                return (LogLevelConfigCollection)base["LogLevels"];
            }
        }
    }
}