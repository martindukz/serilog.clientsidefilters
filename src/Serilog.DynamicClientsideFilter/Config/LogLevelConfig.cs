using System.Configuration;
using Serilog.Events;

namespace Serilog.DynamicClientsideFilter.Config
{
    public class LogLevelConfig : ConfigurationElement
    {
        public LogLevelConfig(string sourceContextPrefix, LogEventLevel level)
        {
            SourceContextPrefix = sourceContextPrefix;
            Level = level;
        }

        public LogLevelConfig()
        {

        }

        [ConfigurationProperty("SourceContextPrefix", IsRequired = true, IsKey = true)]
        public string SourceContextPrefix
        {
            get { return (string)this["SourceContextPrefix"]; }
            set { this["SourceContextPrefix"] = value; }
        }

        [ConfigurationProperty("Level", DefaultValue = LogEventLevel.Information, IsRequired = true, IsKey = false)]
        public LogEventLevel Level
        {
            get { return (LogEventLevel)this["Level"]; }
            set { this["Level"] = value; }
        }
    }
}