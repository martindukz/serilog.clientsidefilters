using System.Configuration;

namespace Serilog.DynamicClientsideFilter.Config
{
    public class LogLevelConfigCollection : ConfigurationElementCollection
    {
        public LogLevelConfigCollection()
        {

        }

        public LogLevelConfig this[int index]
        {
            get { return (LogLevelConfig)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(LogLevelConfig logLevelConfig)
        {
            BaseAdd(logLevelConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new LogLevelConfig();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LogLevelConfig)element).SourceContextPrefix;
        }

        public void Remove(LogLevelConfig logLevelConfig)
        {
            BaseRemove(logLevelConfig.SourceContextPrefix);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }
    }
}