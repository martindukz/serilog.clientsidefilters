using Serilog;

namespace SerilogNamespaceFilterPoC.InfoLogging
{
    public class LogSomethingWhereClassIsSpecificallyTargetedToLessThanNamespace
    {
        public LogSomethingWhereClassIsSpecificallyTargetedToLessThanNamespace()
        {
            ILogger myLogger = Log.Logger.ForContext(GetType());
            myLogger.Debug("this is debug for: " + this.GetType().FullName);
            myLogger.Information("this is information for: " + this.GetType().FullName);
            myLogger.Warning("this is warning for: " + this.GetType().FullName);
            myLogger.Error("this is error for: " + this.GetType().FullName);
        }
    }
}