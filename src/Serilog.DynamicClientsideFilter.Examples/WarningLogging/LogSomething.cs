﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace SerilogNamespaceFilterPoC.WarningLogging
{
    public class LogSomething
    {
        public LogSomething()
        {
            ILogger myLogger = Log.Logger.ForContext(GetType());
            myLogger.Debug("this is debug for: " + this.GetType().FullName);
            myLogger.Information("this is information for: " + this.GetType().FullName);
            myLogger.Warning("this is warning for: " + this.GetType().FullName);
            myLogger.Error("this is error for: " + this.GetType().FullName);
        }
    }
}
