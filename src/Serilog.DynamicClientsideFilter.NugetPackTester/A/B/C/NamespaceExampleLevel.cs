﻿namespace Serilog.DynamicClientsideFilter.NugetPackTester.A.B.C
{
    public class NamespaceExampleLevel
    {
        public static ILogger Logger = Log.Logger.ForContext<NamespaceExampleLevel>();

        public void LogStuff()
        {
            Logger.Debug("this is debug for: " + this.GetType().FullName);
            Logger.Information("this is information for: " + this.GetType().FullName);
            Logger.Warning("this is warning for: " + this.GetType().FullName);
            Logger.Error("this is error for: " + this.GetType().FullName);
            Logger.Fatal("this is fatal for: " + this.GetType().FullName);
        }
    }
}