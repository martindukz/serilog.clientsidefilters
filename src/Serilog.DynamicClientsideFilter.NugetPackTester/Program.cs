﻿using System;
using System.Threading;
using Serilog.Events;
using Serilog.DynamicClientsideFilter;
using Serilog.DynamicClientsideFilter.NugetPackTester.A.B.C;

namespace Serilog.DynamicClientsideFilter.NugetPackTester
{
    public class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Debug()
             .WriteTo.File(@"C:\logs\test\" + DateTime.Now.Ticks + ".log", LogEventLevel.Debug)
             .ApplyDynamicFilter()
             .CreateLogger();

            Log.Logger.Debug("logger configuration is set to debug");

            new ExampleLevelHighest().LogStuff();
            new ExampleLevelLowest().LogStuff();
            new NamespaceExampleLevel().LogStuff();
            Thread.Sleep(1000);
            Console.In.Read();
        }
    }
}
