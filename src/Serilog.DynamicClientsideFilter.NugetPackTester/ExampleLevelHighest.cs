﻿namespace Serilog.DynamicClientsideFilter.NugetPackTester
{
    public class ExampleLevelHighest
    {
        public static ILogger Logger = Log.Logger.ForContext<ExampleLevelHighest>();

        public void LogStuff()
        {
            Logger.Debug("this is debug for: " + this.GetType().FullName);
            Logger.Information("this is information for: " + this.GetType().FullName);
            Logger.Warning("this is warning for: " + this.GetType().FullName);
            Logger.Error("this is error for: " + this.GetType().FullName);
            Logger.Fatal("this is fatal for: " + this.GetType().FullName);
        }
    }
}