﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serilog.DynamicClientsideFilter.NugetPackTester
{
    public class ExampleLevelLowest
    {
        public static ILogger Logger = Log.Logger.ForContext<ExampleLevelLowest>();

        public void LogStuff()
        {
            Logger.Debug("this is debug for: " + this.GetType().FullName);
            Logger.Information("this is information for: " + this.GetType().FullName);
            Logger.Warning("this is warning for: " + this.GetType().FullName);
            Logger.Error("this is error for: " + this.GetType().FullName);
            Logger.Fatal("this is fatal for: " + this.GetType().FullName);
        }
    }
}
