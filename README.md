Configure clientside filter: 
~~~~
<configSections>
	<section name="LogFilterLevelsSection" type="Serilog.DynamicClientsideFilter.Config.LogFilterLevelsSection, Serilog.DynamicClientsideFilter"/>
</configSections>
  <LogFilterLevelsSection softLogLevel="Warning">
    <LogLevels>
      <add SourceContextPrefix="Serilog.DynamicClientsideFilter.NugetPackTester.ExampleLevelLowest" Level="Debug"/>
      <add SourceContextPrefix="Serilog.DynamicClientsideFilter.NugetPackTester.ExampleLevelHighest" Level="Fatal"/>
    </LogLevels>
  </LogFilterLevelsSection>
~~~~

~~~~
using System;
using System.Threading;
using Serilog;
using Serilog.DynamicClientsideFilter;
using Serilog.Events;


namespace SerilogNamespaceFilterPoC
{
    public class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(@"C:\d60\logs\test\" + DateTime.Now.Ticks + ".log", LogEventLevel.Debug)
                .ApplyDynamicFilter()       
                .CreateLogger();

            Log.Logger.Debug("logger configuration is set to debug");

          //  Log.Logger.Information("This first class logs and has been set by namespace to information");
            new InfoLogging.LogSomething();
            //Log.Logger.Information("This second class has been specifcally targeted to reduce to debug.");
            //new InfoLogging.LogSomethingWhereClassIsSpecificallyTargetedToLessThanNamespace();
            //Log.Logger.Information("This third class is within a namespace set to warning within a namespace set to info. (the more specific, i.e. warning, should win)");
            //new InfoLogging.WarnWithinInfo.LogSomething();
            //Log.Logger.Information("This fourth class has namespace target to warning.");
            //new WarningLogging.LogSomething();
            //Log.Logger.Information("This fifth class has namespace target to error.");
            //new OnlyErrors.LogSomething(); 
            Thread.Sleep(1000);
            Console.In.Read();


        }
    }
}
~~~~